package task04_Arrays.priorityqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
     static Logger logger = LogManager.getLogger(task04_Arrays.Main.class);

        public static void startPriorityTask(){

            MyPriorityQueue<String> list = new MyPriorityQueue<>();

            list.add("first",0);
            list.add("second",-1);
            list.add("third",2);

            int size=list.getSize();
            for(int i=0;i<size;i++){
                if(list.canPop()){
                    logger.trace("value: "+ list.getValue());
                }
            }
        }
    }
