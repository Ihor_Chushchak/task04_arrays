package task04_Arrays.priorityqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesSetting {
    final Logger logger = LogManager.getLogger(PropertiesSetting.class);
    final String Way = "game.properties";
    public final String getPropertyValue(final String property) {
        String propertyValue = "";
        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(Way));
            propertyValue = properties.getProperty(property);
        } catch (FileNotFoundException e) {
            logger.error("Property file not found!");
        } catch (IOException e) {
            logger.error("Problems with loading properties!");
        }
        return propertyValue;}
}
