package task04_Arrays;

@FunctionalInterface
public interface Application {
    void start();
}
