package task04_Arrays;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {
    private Map<String, String> menu;
    private Map<String, Application> methodsMenu;
    static Logger logger = LogManager.getLogger(Main.class);

    private static final Scanner scanner = new Scanner(System.in);

    public Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - start Priority Queue Task");
        menu.put("2", " 2 - start Container Task");
        menu.put("3", " 3 - start Array Task");
        menu.put("4", " 4 - Exit");


        methodsMenu.put("1", task04_Arrays.priorityqueue.Main::startPriorityTask);
        methodsMenu.put("2", task04_Arrays.containers.Main::startContainerTask);
        methodsMenu.put("3", task04_Arrays.arrays.Main::startArrayTask);
    }

    private void showMenu() {
        for (String value : menu.values()) {
            logger.trace(value);
        }
    }

    public static void main(String[] args) {

        Main main = new Main();
        String keyMenu;

        do {
            main.showMenu();
            keyMenu = scanner.nextLine();
            logger.trace("Select menu point.");
            try {
                main.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                logger.trace("Error!");
            }
        } while (!keyMenu.equals("4"));
    }
}