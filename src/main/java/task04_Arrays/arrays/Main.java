package task04_Arrays.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task04_Arrays.priorityqueue.PropertiesSetting;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static Logger logger = LogManager.getLogger(task04_Arrays.Main.class);

    public void fillRand(int[] mas) {
        Random rnd = new Random();
        final int min = 1;
        final int max = 10;
        for (int i = 0; i < mas.length; i++) {
            mas[i] = min + rnd.nextInt(max);
        }
    }

    public int[] getSameNumbersFromArrays(int[] firstArray, int[] secondArray) {
        int sameNumbersArraySize = 0;
        int firstIterator = 0;
        int[] sameNumbersArray;
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j]) {
                    sameNumbersArraySize++;
                }
            }
        }
        sameNumbersArray = new int[sameNumbersArraySize];
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j]) {
                    sameNumbersArray[firstIterator] = firstArray[i];
                    firstIterator++;
                }
            }
        }
        return sameNumbersArray;

    }

    private int[] getUniqueElementsFromOtherArray(int[] firstArray, int[] secondArray) {
        int[] indexUnique = new int[firstArray.length];
        Arrays.fill(indexUnique, -1);
        for (int i = 0; i < firstArray.length; i++) {
            for (int j : secondArray) {
                if (firstArray[i] == j)
                    indexUnique[i]++;
            }
        }
        int countUnique = 0;
        for (int i : indexUnique)
            if (i == -1)
                countUnique++;
        int[] exitArray = new int[countUnique];
        for (int i = 0; i < indexUnique.length; i++) {
            if (indexUnique[i] == -1)
                exitArray[countUnique-- - 1] = firstArray[i];
        }
        return exitArray;
    }


    public int[] removeRepeated(int[] firstArray) {
        if (firstArray == null || firstArray.length == 0) return firstArray;
        Arrays.sort(firstArray);
        int n = 1;
        for (int i = 1; i < firstArray.length; i++) {
            if (firstArray[i] != firstArray[i - 1]) {
                n++;}
        }
        int[] exitArray = new int[n];
        exitArray[0] = firstArray[0];
        n = 1;
        for (int i = 1; i < firstArray.length; i++) {
            if (firstArray[i] != firstArray[i - 1]) exitArray[n++] = firstArray[i];
        }
        return exitArray;

    }

    private int[] removeDuplicates(int[] firstArray) {
        int[] exitArray;
        int[] secondArray = new int[firstArray.length];
        int i = 0,
                ind = 1,
                k = 0;
        boolean isDuplicate;

        while (i < firstArray.length) {
            isDuplicate = false;
            for (int j = i + 1; j < firstArray.length; j++) {
                if (firstArray[i] == firstArray[j]) {
                    ind = j;
                    secondArray[k] = firstArray[j];
                    isDuplicate = true;
                }
            }
            if (!isDuplicate)
                secondArray[k] = firstArray[i];
            else {
                i = ind;
                isDuplicate = false;
            }
            i++;
            k++;
        }

        exitArray = new int[k];

        for (int l = 0; l < k; l++)
            exitArray[l] = secondArray[l];

        return exitArray;
    }

    public static void startArrayTask() {
        Scanner scanner = new Scanner(System.in);
        logger.trace("Enter first array lenth:");
        final int firstArraySize = scanner.nextInt();
        logger.trace("Enter second array lenth:");
        final int secondArraySize = scanner.nextInt();
        int[] firstArray = new int[firstArraySize];
        int[] secondArray = new int[secondArraySize];
        Main main = new Main();
        main.fillRand(firstArray);
        main.fillRand(secondArray);
        logger.trace("Generated first array: " + Arrays.toString(firstArray));
        logger.trace("Generated second array: " + Arrays.toString(secondArray));
        logger.trace("Array consist of same numbers from both arrays: " + Arrays.toString(main.getSameNumbersFromArrays(firstArray, secondArray)));
        logger.trace("Array consist of unique numbers: " + Arrays.toString(main.getUniqueElementsFromOtherArray(firstArray, secondArray)));
        logger.trace("Array consist without duplicates: " + Arrays.toString(main.removeDuplicates(firstArray)));
        logger.trace("Array without repeated numbers more than twice :" + Arrays.toString(main.removeRepeated(firstArray)));
        logger.trace("-------------------------------");
        Game game = new Game();
        final int gameArraySize= Integer.valueOf(
                new PropertiesSetting().getPropertyValue("gameArraySize"));
        int counter;
        int minMonsterPower;
        int maxMonsterPower;
        int minArtifactPower;
        int maxArtifactPower;
        counter = 0;
        minMonsterPower = 5;
        maxMonsterPower = 100;
        minArtifactPower = 20;
        maxArtifactPower = 80;
        int[] gamePlay;
        int[] gamePoints;
        gamePlay = new int[gameArraySize];
        gamePoints = new int[gameArraySize];
        logger.trace("Welcome: Lets get started!");

        for (int i = 0; i < gameArraySize; i++) {
            gamePlay[i] = game.rnd(0, 1);
        }
        for (int i = 0; i < gameArraySize; i++) {
            if (gamePlay[i] == 0) {
                gamePoints[i] = game.rnd(minMonsterPower,maxMonsterPower);
            } else {
                gamePoints[i] = game.rnd(minArtifactPower, maxArtifactPower);
            }
        }
        for (int i = 0; i < gameArraySize; i++) {
            if (gamePlay[i] == 0) {
                logger.trace(i + " - Monster  - with power of " + gamePoints[i] + " points");
            } else {
                logger.trace(i + " - Artifact - with power of " + gamePoints[i] + " points");
            }
        }
        logger.trace("There are " + game.getNumOfDeat(gamePlay, gamePoints, counter) + " doors where you can die! :(");
        if (game.checkIfPossibleWin(gamePlay, gamePoints)) {
            logger.trace("Congratulations: you have possibility to win!");
            game.findWinWay(gamePlay);
        } else {
            logger.trace("Unfortunately: there is no way to win here!");
            System.exit(1);
        }

    }
}
