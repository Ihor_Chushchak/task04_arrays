package task04_Arrays.arrays;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task04_Arrays.Main;
import task04_Arrays.priorityqueue.PropertiesSetting;

public class Game {
        int iterator = 0;
        int player = Integer.valueOf(
                new PropertiesSetting().getPropertyValue("player"));
        static Logger logger = LogManager.getLogger(Main.class);
        public int rnd(int min, int max)
        {
            max -= min;
            return (int) (Math.random() * ++max) + min;
        }
        public int getNumOfDeath(int[] gamePlay, int[] gamePoints,int counter) {
            if (gamePlay[iterator]==1){
                iterator++;
            }
            else if (gamePlay[iterator]==0){
                if(player>=gamePoints[iterator]){
                    iterator++;
                    counter = getNumOfDeath(gamePlay,gamePoints,counter);
                }
                else if(player<gamePoints[iterator]){
                    iterator++;
                    counter = getNumOfDeath(gamePlay,gamePoints,counter+1);}

            }
            return counter; }
        public int getNumOfDeat(int[] gamePlay, int[] gamePoints,int counter) {
            for (int i=0;i<gamePlay.length;i++){
                if (gamePlay[i]==0){
                    if (player < gamePoints[i]) {
                        counter++;
                    }
                }
            }
            return counter;
        }
        public boolean checkIfPossibleWin(int[] gamePlay, int[] gamePoints){
            int artifactSum;
            int monsterSum;
            artifactSum = 0;
            monsterSum = 0;
            for (int i=0;i<gamePlay.length;i++){
                if (gamePlay[i] == 0) {
                    monsterSum = monsterSum + gamePoints[i];
                } else {
                    artifactSum = artifactSum + gamePoints[i];
                }
            }
            if(artifactSum>=monsterSum){
                return true;
            }
            return false;
        }
        public void findWinWay(int[] gamePlay){
            logger.trace("To pass all monsters you are better to follow next doors:\nFrom->");
            for (int i=0;i<gamePlay.length;i++){
                if (gamePlay[i] == 1 ) {
                    logger.trace(i+"-to->");
                }}
            for (int i=0;i<gamePlay.length;i++){
                if (gamePlay[i] == 0 ) {
                    logger.trace(i+"-to->");
                }
            }
            logger.trace("And you will Win!");
        }

    }


