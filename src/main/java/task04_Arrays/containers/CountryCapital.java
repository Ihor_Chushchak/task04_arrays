package task04_Arrays.containers;

public class CountryCapital {

        private String country;
        private String capital;
        public CountryCapital(){

        }

        public CountryCapital(String country, String capital) {
            this.country = country;
            this.capital = capital;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCapital() {
            return capital;
        }

        public void setCapital(String capital) {
            this.capital = capital;
        }

        public void setAll(CountryCapital countryCapital){
            this.country = countryCapital.country;
            this.capital = countryCapital.capital;
        }

        @Override
        public String toString() {
            return "Country: " + country + "\nCapital: " + capital + "\n----------------";
        }
    }


