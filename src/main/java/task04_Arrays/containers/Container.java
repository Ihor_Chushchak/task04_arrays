package task04_Arrays.containers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task04_Arrays.priorityqueue.PropertiesSetting;

public class Container {
    private static final int MAX_SIZE = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("gameArraySize"));
    static Logger logger = LogManager.getLogger(task04_Arrays.Main.class);
    private static int size;

    private String[] array ;
    private int index;

    public Container(){
        array = new String[MAX_SIZE];
        index=0;
        size=MAX_SIZE;
    }
    public Container(int size){
        Container.size=size*2;
        array = new String[Container.size];
        index=0;
    }

    private boolean isCorrect(){
        return index<size;
    }

    private void reInit(){
        String[] clone;
        clone = array.clone();

        size *= 2;
        array = new String[size];
        for(int i=0;i<clone.length;i++){
            array[i]=clone[i];
        }
    }

    public void add(String obj){
        if(isCorrect()){
            array[index] = obj;
            index++;
        }
        else{
            reInit();
            array[index] = obj;
            index++;
        }
    }
    public void setIndexToStart(){
        index=0;
    }
    public String getElement(){
        return array[index];
    }

    public void showAll(){
        for(int i=0;i<size;i++){
            if(array[i]!=null){
                logger.trace("value["+i+"]: "+array[i]);
            }
            else{
                break;
            }
        }
    }
}
